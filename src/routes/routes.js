const { Router } = require('express');
const addressController = require('../controllers/AddressController/AddressController')
const weatherController = require('../controllers/m3oWeatherController/m3oWeatherController')
const userController = require('../controllers/UserController/UserController');
const router = Router();


//authentication uses
const authController = require("../controllers/AuthController/AuthController");
const passport = require("passport");
router.use("/private", passport.authenticate('jwt', {session: false}));

//authentication routes
router.post("/login", authController.login);
router.get("/private/getDetails", authController.getDetails);



//routes for user
router.get('/users', userController.index);
router.get('/users/:id',userController.show);
router.post('/users',userController.create);
router.put('/users/:id',userController.update);
router.delete('/users/:id',userController.destroy);

//routes for address
router.get('/address', addressController.index);
router.get('/address/:id',addressController.show);
router.post('/address',addressController.create);
router.put('/address/:id',addressController.update);
router.delete('/address/:id',addressController.destroy);
router.put('/address/:AddressId/user/:UserId', addressController.addAddress);

//route for get the weather
router.get('/weather/:id', weatherController.getWeatherInformation);


module.exports = router;
