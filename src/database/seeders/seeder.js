require('../../config/dotenv')();
require('../../config/sequelize');

//const seedModel = require('./Model');
const seedUser = require('./UserSeeder');

(async () => {
  try {
    await seedUser();
    //await seedModel();

  } catch(err) { console.log(err) }
})();
