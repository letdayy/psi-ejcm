const Address  = require('../../models/address');
const User = require('../../models/user');

const create = async(req, res) => {
    try {
        const address = await Address.create(req.body);
        return res.status(200).json({address: address});
    } catch (error) {
        return res.status(500).json(error);
    }
};


const index = async(req, res) => {
    try {
        const address = await Address.findAll();
        return res.status(200).json(address);
    }
    catch(err) {
        res.status(500).json({error: err});
    }
};

const show = async(req, res) => {
    const {id} = req.params;
    try {
        const address = await Address.findByPk(id);
        return res.status(200).json(address);
    }
    catch(err) {
        res.status(500).json(`${err}!`);
    }
};

const update = async(req, res) => {
    const {id} = req.params;
    try {
        const [update] = await Address.update(req.body, {where: {id: id}});
        if(update) {
            const address = await Address.findByPk(id);
            return res.status(200).send(address);
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};

const destroy = async(req,res) => {
    const{id} = req.params;
    try {
        const deleted = await Address.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};

const addAddress = async(req, res) => {
    const {UserId, AddressId} = req.params;

    try {
        const user = await User.findByPk(UserId);
        const address = await Address.findByPk(AddressId)
        await address.setUser(user);

        return res.status(200).json(address)
    } catch (error) {
        return res.status(500).json(error)
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addAddress
};
