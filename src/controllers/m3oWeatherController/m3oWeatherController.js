const Address = require('../../models/address');
const m3o = require('m3o').default(process.env.M3O_API_TOKEN);
const Weather = m3o.weather;

const getWeatherInformation = async(req, res) => {
    const {id} = req.params;
    try {
        const address = await Address.findByPk(id);
        const weather = await Weather.now({
            location: address.location
        });
        return res.status(200).json({weather: weather});
    } catch (error) {
        return res.status(500).json(error);
    };
};


module.exports = {getWeatherInformation};