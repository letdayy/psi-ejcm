const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const address = sequelize.define('address', {
    country: {
        type: DataTypes.STRING,
        allowNull: false
        },
    location: {
        type: DataTypes.STRING,
        allowNull: false
        },
    zipcode: {
        type: DataTypes.STRING,
        allowNull: false
        }
});


address.associate = function(models) {
    address.belongsTo(models.user);
};



module.exports = address;