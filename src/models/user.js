const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const user = sequelize.define('user', {
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
        },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false
        },
    email: {
        type: DataTypes.STRING,
        allowNull: false
        },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
        },
    birthday: {
        type: DataTypes.DATEONLY,
        allowNull: false
        },
    hash: {
        type: DataTypes.STRING,
        allowNull: false
        },
    salt: {
        type: DataTypes.STRING,
        allowNull: false
        },
    
});

user.associate = function(models) {
     user.hasOne(models.address);
};

module.exports = user;
